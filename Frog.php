<?php

class Frog extends Animals{
    public  $jump;

    public function __construct($name = "data", $legs = 4, $cold_blooded = "no", $jump = "Hop-Hop"){
        parent::__construct($name, $legs, $cold_blooded);
        $this->jump = $jump;
    }

    public function jump(){
        $str = "<br>" . parent::getAllLabel() . "Jump : {$this->jump}<br>";
        return $str;
    }
}