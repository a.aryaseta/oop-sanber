<?php

require("Animals.php");
require("Frog.php");
require("Ape.php");

$sheep = new Animals("shaun");
$frog = new Frog("buduk");
$ape = new Ape("kera sakti");

echo $sheep->getAllLabel();
echo $frog->jump();
echo $ape->yell();

