<?php

class Ape extends Animals{
    public  $yell;

    public function __construct($name = "data", $legs = 2, $cold_blooded = "no", $yell = "Auooo"){
        parent::__construct($name, $legs, $cold_blooded);
        $this->yell = $yell;
    }

    public function yell(){
        $str = "<br>" . parent::getAllLabel() . "Yell : {$this->yell}";
        return $str;
    }
}