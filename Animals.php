<?php

class Animals{
    public  $name;
    public  $legs;
    public  $cold_blooded;

    public function __construct($name = "data", $legs = 4, $cold_blooded = "no"){
        $this->name 		    = $name;
		$this->legs 		    = $legs;
		$this->cold_blooded 	= $cold_blooded;
    }
        
    public function getAllLabel(){
        $str = "Nama : {$this->name}<br> Legs : {$this->legs}<br> Cold Blooded : {$this->cold_blooded}<br>";
        return $str;
    }
}